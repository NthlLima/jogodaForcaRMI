import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.Naming;

public class JogodaForcaCliente {
    private JogodaForca forca;
    private BufferedReader buffer;
    private static String letrasUsadas = "";
    
    JogodaForcaCliente() {
        buffer = new BufferedReader(new InputStreamReader(System.in));
        
        try {
            forca = (JogodaForca) Naming.lookup("rmi://192.168.0.101:1099/JogodaForcaService"); 
            
        } catch (Exception e) {            
            e.printStackTrace();
        }
    }
    
    public String getLetrasUnicas(String frase){
        String letras = "";
        
        for (int i = 0; i < frase.length(); i++) {
            char letra = frase.charAt(i);
            
            if (letra != ' ') {
                boolean flag = false;
                for (int j = 0; j < letras.length(); j++) {
                    if (letras.charAt(j) == letra) {
                        flag = true;
                        break;
                    }
                }               
                if (!flag) letras += "" + letra;                
            }
        }

        return letras;
    }
    
    public boolean verificarEntrada(String entrada) {        
        char letra  = ' ';
        entrada = entrada.toUpperCase().trim();                
        
        if (entrada.length() >= 1) {
            letra = entrada.charAt(0);            
            if (!("" + letra).matches("[A-Z]")) {
                return false;
            }

            for (int i = 0; i < letrasUsadas.length(); i++) {
                if (letrasUsadas.charAt(i) == letra) {
                    return false;
                }
            }
            
        } else {
            return false;
        }
                
        letrasUsadas += letra;       
        return true;
    }
    
    public String descriptografarFrase(String frase, char encript) {
        String let = "";         
        
        for (int i = 0; i < frase.length(); i++) {
            let += (frase.charAt(i) == ' ') ? ' ' : encript;
        }
                        
        for (int i = 0; i < letrasUsadas.length(); i++) {
            char letra = letrasUsadas.charAt(i);
            
            for (int j = 0; j < frase.length(); j++) {                
                if (letra == frase.charAt(j)) {                    
                    char[] vetorLetras = let.toCharArray();
                    vetorLetras[j] = letra;
                    let = new String(vetorLetras);                    
                }
            }            
        }
                        
        return let;
    }
    
    public void finalizarJogo() {
        try {
            System.out.print("Gostaria de jogar novamente? (Insira 'N' ou 'n' então pressione <Enter> para finalizar o jogo!): ");
            String input = buffer.readLine();
            
            if (input.trim().toUpperCase().equals("N")) {
                System.exit(0);
            }
            
        } catch (IOException e) {
            finalizarJogo();
        }
    }
    
    public void jogar(String frase){        
        String letras = getLetrasUnicas(frase);        
        int chances = (letras.length() < 21) 
                ? letras.length() + 5 
                : letras.length() + (26 - letras.length());
        
        int i = 0;        
        for (i = chances; i > 0; i--) {
            
            System.out.print("\n");                       
            System.out.println(" --------------------------- ");            
            System.out.println("|  Número de tentativas: " + i + "  |");            
            System.out.println(" --------------------------- ");
            System.out.print("\n");
            System.out.println("A frase é:");
            System.out.println(descriptografarFrase(frase, '*'));
            System.out.println("Insira uma letra");
            
            String entrada = "";
            while (entrada.equals("")) {
                try {                    
                    System.out.print(">");
                    entrada = buffer.readLine();
                    
                    if (verificarEntrada(entrada)) {                       
                        if (descriptografarFrase(frase, '*').equals(frase)) {
                            System.out.println("\nParabéns você ganhou! Bom trabalho!");
                            System.out.println(">" + descriptografarFrase(frase, '*'));
                            System.out.println("\n");
                            finalizarJogo();
                        }
                        
                    } else {
                        throw new IOException("Ocorreu um erro na letra de entrada.");
                    }
                } catch (IOException e) {                    
                    entrada = "";
                    continue;
                }
            }                        
        }
        
        letrasUsadas = "";
            
        if (i <= 0) {
            System.out.println("\nVocê perdeu! Mais sorte na próxima!");
            finalizarJogo();
        }
    }
    
    public void telaInicial() {
        System.out.println("\nBem vindo ao Jogo da Forca com RMI!\n");
        System.out.println(
            "Insira uma letra e em seguida pressione <Enter>\n" +
            "Se tiver mais de uma letra apenas a primeira letra será válida");
        System.out.println("E a dica é: DITADOS POPULARES\n");
        System.out.println("Boa Sorte!");         
    }
    
    public static void main(String[] args) throws Exception {
        JogodaForcaCliente cliente = null;       
        
        cliente = new JogodaForcaCliente();
        cliente.telaInicial();
        
        for (;;) {
            String frase = cliente.forca.getFraseRandomica();
            cliente.jogar(frase.toUpperCase());
        }
    }
}
