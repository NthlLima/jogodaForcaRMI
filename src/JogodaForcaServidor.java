import java.rmi.Naming; 
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;

public class JogodaForcaServidor {
    JogodaForcaServidor() {
        System.out.println("Inicializando Jogo da Forca...");
        
        try {
            System.setProperty("java.rmi.serve.hostname", "192.168.0.101");
            LocateRegistry.createRegistry(1099);
            
            JogodaForca f = new JogodaForcaMain();
            Naming.bind("JogodaForcaService", (Remote) f);
            
            System.out.println("Servidor do Jogo da Forca está sendo executado.");
            
        } catch (Exception e) { 
            e.printStackTrace(); 
        }
    }
    public static void main(String[] args) { 
        new JogodaForcaServidor(); 
    } 
}
