import java.rmi.Remote;
import java.rmi.RemoteException;

public interface JogodaForca extends Remote {
    public String getFraseRandomica() throws RemoteException;
}
