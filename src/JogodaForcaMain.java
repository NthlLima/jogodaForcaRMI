import java.rmi.RemoteException; 
import java.rmi.server.UnicastRemoteObject; 

public class JogodaForcaMain extends UnicastRemoteObject implements JogodaForca {
    protected JogodaForcaMain() throws RemoteException { 
        super(); 
    }
    
    public String getFraseRandomica() throws RemoteException {
        int index = (int) (Math.random() * (frases.length - 1));
        return frases[index];
    }
    
    private String[] frases = {
        "Para bom entendedor meia palavra basta",
        "De grao em grao a galinha enche o papo",
        "Cada macaco no seu galho",
        "Casa de ferreiro espeto de pau",
        "Agua mole pedra dura tanto bate ate que fura",
        "Filho de peixe peixinho e",
        "Deus ajuda quem cedo madruga",
        "Onde ha fumaca ha fogo",
        "Cao que ladra nao morde",
        "Pimenta nos olhos dos outros e refresco",
        "Por ele eu ponho minha mao no fogo",
        "Quando um burro fala o outro abaixa a orelha",
        "A pressa e inimiga da perfeicao",
        "A noite todos os gatos sao pardos",
        "Deus escreve certo por linhas tortas",
        "Quem com ferro fere com ferro sera ferido",
        "Um dia e da caca outro do cacador",
        "Mente vazia oficina do diabo",
        "O que os olhos nao veem, o coracao nao sente",
        "Papagaio que acompanha joao de barro vira ajudante de pedreiro",
        "De medico e louco todo mundo tem um pouco",
        "A Cesar o que e de Cesar",
        "Cavalo dado nao se olha os dentes",
        "Em terra de cego, quem tem um olho e rei",
        "Ladrao que rouba ladrao tem cem anos de perdao",
        "O seguro morreu de velho",
        "Cada panela tem sua tampa",
        "Nao grite a sua felicidade pois a inveja tem sono leve",
        "Quem canta seus males espanta",
        "Uma andorinha sozinha nao faz verao",
        "O habito faz o monge",
        "Para baixo todo santo ajuda",
        "Quem ama o feio bonito lhe parece",
        "Quem sebmistura com porcos farelo come",
        "Quem ri por ultimo ri melhor",
        "Quem semeia vento colhe tempestade",
        "Saco vazio nao para em pe",
        "Quem tem boca vaia Roma",
        "Quem casa quer casa",
        "Gato escaldado tem medo de água fria"
    };
}
