# Jogo da Forca utilizando RMI
*Antes de iniciar qualquer processo é necessário ter o JDK previamente instalado*

## Gerando Arquivos `.class`
Na pasta `src`:

```
	javac JogodaForca.java
	javac JogodaForcaCliente.java
	javac JogodaForcaMain.java
	javac JogodaForcaServidor.java
```

## Iniciando Servidor

```
	rmic JogodaForcaMain
	java JogodaForcaServidor
```

## Iniciando o Jogo
Após iniciar o servidor, utilizar um novo `cmd`.
```
	javac JogodaForcaCliente
```

Após um jogo iniciado, informações restantes na própria aplicação.